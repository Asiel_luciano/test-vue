import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    typePunch:[
      {
        code:"IN",
        name:"ENTRADA",
        aviableQuantity:1,
        aviable:true
      },
      {
        code:"OUT",
        name:"SALIDA",
        aviableQuantity:1,
        aviable:true
      },
      {
        code:"BK",
        name:"BREAK",
        aviableQuantity:2,
        aviable:true
      },
      {
        code:"LCH",
        name:"ALMUERZO",
        aviableQuantity:1,
        aviable:true
      },


    ],
    
    employe:localStorage.getItem('test-vue-employe')?JSON.parse(localStorage.getItem('test-vue-employe')):[],
    employeToShow:[]


  },
  mutations: {

    addEmploye(state,employe){
     // console.log(employe)
      //employe.id=employe.id+10
      state.employeToShow.push(employe)
       //employe.id=employe.id-10
     state.employe.push(employe)
     console.log(state.employe)
       localStorage.setItem('test-vue-employe',JSON.stringify(state.employeToShow))
  
    },
    updateEmployes(state,data){
    console.log(data.type)
    
  
       if(data.type=='BK'){
        
          state.employe[data.id-1].status='BK'
        state.employe[data.id-1].typePuch.break=state.employe[data.id-1].typePuch.break-1
        
         
    //   data.employe.typePuch.break=state.employe[data.id-1].typePuch.break-1
        
       }else if(data.type=='LCH'){
         
        state.employe[data.id-1].status='LCH'
        state.employe[data.id-1].typePuch.food=state.employe[data.id-1].typePuch.food-1    
      //data.employe.typePuch.food=state.employe[data.id-1].typePuch.food-1
       }
       else if(data.type=='IN'){
        state.employe[data.id-1].status='IN'
        state.employe[data.id-1].typePuch.in=state.employe[data.id-1].typePuch.in-1     
       //  data.employe.typePuch.in=state.employe[data.id-1].typePuch.in-1   
       }
        else if(data.type=='OUT'){
        state.employe[data.id-1].status='OUT'
        state.employe[data.id-1].typePuch.out=state.employe[data.id-1].typePuch.out-1   
       //  data.employe.typePuch.out =state.employe[data.id-1].typePuch.out-1        
       }
       state.employeToShow.push(data.employe)
      state.employe.push(state.employe[data.id-1])
       localStorage.setItem('test-vue-employe',JSON.stringify(state.employeToShow))
    },
   
  },
  actions: {
  },
  modules: {
  }
})
